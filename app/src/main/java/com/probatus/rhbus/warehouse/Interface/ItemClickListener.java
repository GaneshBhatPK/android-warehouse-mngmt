package com.probatus.rhbus.warehouse.Interface;

import android.view.View;

/**
 * Created by Ganapathi on 13-03-2018.
 */

public interface ItemClickListener {
    public void onClick(View view, int position);
}
