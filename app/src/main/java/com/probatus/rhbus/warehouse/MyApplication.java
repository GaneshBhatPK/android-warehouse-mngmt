package com.probatus.rhbus.warehouse;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.ViewGroup;

import com.irozon.sneaker.Sneaker;

import java.util.ArrayList;

import androidx.multidex.MultiDex;

/**
 * Created by ganesh on 11/6/18.
 */

public class MyApplication extends Application {

    private static MyApplication mInstance;
    private ArrayList someVariable;
    private boolean isFirstTime;
    private String docno,date;

    public static final int ERROR = 1;
    public static final int SUCCESS = 2;
    public static final int WARNING = 3;
    public static final int INFO = 4;

    public ArrayList getSomeVariable() {
        return someVariable;
    }

    public void setSomeVariable(ArrayList someVariable) {
        this.someVariable = someVariable;
    }

    public boolean getFirsttimeStatus() {
        return isFirstTime;
    }

    public void setFirsttimeStatus(boolean isFirstTime) {
        this.isFirstTime = isFirstTime;
    }

    public String getDocumentNumber(){ return docno;}

    public void setDocumentNumber(String docno){this.docno = docno;}

    public String getDate(){ return date;}

    public void setDate(String date){this.date = date;}





    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    @Override
    protected void attachBaseContext(Context base){
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static void displaySnackBar(Activity activity, int type, String message) {
        try {
            switch (type) {
                case MyApplication.ERROR:
                    Sneaker.with(activity)
                            .setTitle("Error!!")
                            .setMessage(message)
                            .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .sneakError();
                    break;
                case MyApplication.SUCCESS:
                    Sneaker.with(activity)
                            .setTitle("Success!!")
                            .setMessage(message)
                            .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .sneakSuccess();
                    break;
                case MyApplication.WARNING:
                    Sneaker.with(activity)
                            .setTitle("Warning!!")
                            .setMessage(message)
                            .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .sneakWarning();
                    break;
                default:
                    Sneaker.with(activity)
                            .setTitle("Info!!")
                            .setMessage(message)
                            .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                            .sneak(R.color.colorInfo);
                    break;
            }
        }catch (Exception e){}
    }
}
