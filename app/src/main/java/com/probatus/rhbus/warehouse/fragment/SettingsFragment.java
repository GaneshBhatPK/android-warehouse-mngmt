package com.probatus.rhbus.warehouse.fragment;

import android.Manifest;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import com.dcastalia.localappupdate.DownloadApk;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.probatus.rhbus.warehouse.Interface.INavigationFragment;
import com.probatus.rhbus.warehouse.MyApplication;
import com.probatus.rhbus.warehouse.R;
import com.thefinestartist.finestwebview.FinestWebView;
import com.webianks.easy_feedback.EasyFeedback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import cz.msebera.android.httpclient.Header;
import lecho.cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by ganapathi on 23/1/20.
 */

public class SettingsFragment extends PreferenceFragment
        implements INavigationFragment {

    protected FloatingActionButton fab;
    private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1001;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        Preference prefCheckForUpdates = findPreference("myKey");

        Preference prefreportKey = findPreference("reportKey");

        Preference prefhelpKey = findPreference("helpKey");

        Preference prefcontactKey = findPreference("contactKey");

        Preference prefaboutKey = findPreference("aboutKey");

        prefhelpKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new FinestWebView.Builder(getActivity()).show(getActivity().getResources().getString(R.string.helppage));
                return false;
            }
        });

        prefreportKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new EasyFeedback.Builder(getActivity())
                        .withEmail(getActivity().getResources().getString(R.string.contactEmail))
                        .build()
                        .start();
                return false;
            }
        });

        prefaboutKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new FinestWebView.Builder(getActivity()).show(getActivity().getResources().getString(R.string.homepage));
                return false;
            }
        });

        prefcontactKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new FinestWebView.Builder(getActivity()).show(getActivity().getResources().getString(R.string.contactpage));
                return false;
            }
        });

        prefCheckForUpdates.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AsyncHttpClient client = new AsyncHttpClient();

                SweetAlertDialog builder = new SweetAlertDialog(getActivity(),
                        SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Update ?")
                        .setContentText("Check for Updates ?")
                        .setConfirmText("UPDATE")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                client.get(getActivity(), getActivity().getResources().getString(R.string.hitgithubCommon), null, new JsonHttpResponseHandler() {

                                    @Override
                                    public void onStart() {
                                        Log.e("Tag", "Started");
                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        super.onSuccess(statusCode, headers, response);
                                        try {
                                            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                                            if(response.getString("latestVersion").compareTo(pInfo.versionName) > 0){
                                                MyApplication.displaySnackBar(getActivity(),MyApplication.INFO,
                                                        "Auto Download Started for " + response.getString("latestVersion") + " :\n"+ response.getString("releaseVersion"));
                                                checkWriteExternalStoragePermission();
                                            } else {
                                                MyApplication.displaySnackBar(getActivity(),MyApplication.INFO, "No Updates Available! for "+ pInfo.versionName +", App is Upto Date :"+
                                                        response.getString("latestVersion"));
                                            }
                                        } catch (PackageManager.NameNotFoundException e) {
                                            MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR, ""+ e);
                                        } catch (JSONException e) {
                                            MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR, ""+ e);
                                        }
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                                        super.onFailure(statusCode, headers, throwable, errorResponse);
                                        MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR,
                                                "Please Try Again After Some Time, Error Status: " + statusCode + throwable);
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                                        super.onFailure(statusCode, headers,  throwable, errorResponse);
                                        MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR,
                                                "Please Try Again After Some Time, Error Status: " + statusCode + throwable);
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable){
                                        super.onFailure(statusCode, headers, responseString, throwable);
                                        MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR,
                                                "Please Try Again After Some Time, Error Status: " + statusCode + throwable);
                                    }
                                });
                            }
                        });
                builder.setCancelText("CANCEL")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        });
                builder.setNeutralText("GITHUB")
                        .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                checkWriteExternalStoragePermission();
                            }
                        });
                builder.show();
                return true;
            }
        });

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle(getResources().getString(R.string.titleSettings));

        ListView listView = (ListView) view.findViewById(android.R.id.list);
        if (listView != null) listView.setVerticalScrollBarEnabled(false);
        prepareFloatingActionButton();

    }

    protected void prepareFloatingActionButton(){
        if(fab == null){
            fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        }
        fab.hide();
    }

    private void checkWriteExternalStoragePermission() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            /** If we have permission than we can Start the Download the task **/
            downloadTask();
        } else {
            /** If we don't have permission than requesting  the permission **/
            requestWriteExternalStoragePermission();
        }
    }

    private void requestWriteExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),  new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else{
            ActivityCompat.requestPermissions(getActivity(),new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            downloadTask();
        } else {
            MyApplication.displaySnackBar(getActivity() ,MyApplication.ERROR, "Permission Not Granted.");
        }
    }

    private void downloadTask() {
        /** This @DownloadApk class is provided by our Library **/
        /** Pass the  Context when creating object of DownlodApk **
         */

        DownloadApk downloadApk = new DownloadApk(getActivity());

        /** For Starting download call the method startDownLoadingApk() by passing the URL **/
        downloadApk.startDownloadingApk(getActivity().getResources().getString(R.string.hitgithubdownload));
    }
}
