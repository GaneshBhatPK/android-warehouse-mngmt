package com.probatus.rhbus.warehouse;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.probatus.rhbus.warehouse.Adapter.UserAdapter;
import com.probatus.rhbus.warehouse.Interface.ItemClickListener;
import com.probatus.rhbus.warehouse.JsonParse.HttpResponse;
import com.probatus.rhbus.warehouse.JsonParse.JsonResponse;
import com.probatus.rhbus.warehouse.JsonParse.MyStatic;
import com.webianks.easy_feedback.EasyFeedback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.appwise.components.ni.NoInternetDialog;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import lecho.cn.pedant.SweetAlert.SweetAlertDialog;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by ganapathi on 24/8/18.
 */

public class UserActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ConnectivityReceiver.ConnectivityReceiverListener,
        ItemClickListener {

    LayoutAnimationController animation;
    FloatingActionButton fab;
    SwipeRefreshLayout refreshCollection;
    RecyclerView listView;

    public JsonResponse jsonResponse = new JsonResponse();
    ArrayList<String> sampleListView = new ArrayList<String>();
    ArrayList<String> list = new ArrayList<String>();
    NoInternetDialog noInternetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);

        listView = (RecyclerView) findViewById(R.id.sampleListView);
        refreshCollection = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        refreshCollection.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPlease();
            }
        });

        setUserView();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        noInternetDialog = new NoInternetDialog.Builder(this).build();
    }

    @Override
    public void onClick(final View view, int position) {
        if (position == 0) {
            MyStatic.setWhichactivity("GOODSRECIEPT");
            this.startActivityForResult(new Intent(UserActivity.this, HomeViewActivity.class), 1);
        } else if (position == 1) {
            MyStatic.setWhichactivity("PICKORDER");
            this.startActivityForResult(new Intent(UserActivity.this, HomeViewActivity.class), 1);
        } else if (position == 2) {

        } else if (position == 3) {
            MyStatic.setWhichactivity("TRANSFER");
            this.startActivityForResult(new Intent(UserActivity.this, HomeViewActivity.class), 1);
        } else if (position == 4) {
            MyStatic.setWhichactivity("SETTING");
            this.startActivityForResult(new Intent(UserActivity.this, NavigationActivity.class), 1);
        }
    }


    public void setUserView() {
        refreshCollection.setRefreshing(true);

        list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        if (checkConnection()) {
            this.jsonResponse.JsonResponse(UserActivity.this, "UserAppMenu_android", "UserAppMenu_getList", list, new HttpResponse<JSONObject>() {
                public void onResponse(final JSONObject grheader) throws JSONException {
                    sampleListView = new ArrayList<String>();
                    try {
                        JSONArray responce = grheader.getJSONArray("result");
                        JSONArray grCount = responce.getJSONArray(0);
                        JSONArray pickCount = responce.getJSONArray(1);
                        JSONArray trasCount = responce.getJSONArray(2);
                        sampleListView.add("Goods Receipt ( " + grCount.getJSONObject(0).getString("countgr") + " )");
                        sampleListView.add("Pick Order ( " + pickCount.getJSONObject(0).getString("countpick") + " )");
                        sampleListView.add("Movements ( 0 )");
                        sampleListView.add("Transfer ( " + trasCount.getJSONObject(0).getString("counttrans") + " )");
                        sampleListView.add("Setting");
                    } catch (Exception e) {
                        sampleListView.add("Goods Receipt ( 0 )");
                        sampleListView.add("Pick Order ( 0 )");
                        sampleListView.add("Movements ( 0 )");
                        sampleListView.add("Transfer ( 0 )");
                        sampleListView.add("Setting");
                        MyApplication.displaySnackBar(UserActivity.this, MyApplication.ERROR, "" + grheader.getString("result"));
                    }
                    LinearLayoutManager llm = new LinearLayoutManager(UserActivity.this);
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    listView.setLayoutManager(llm);
                    UserAdapter adapter = new UserAdapter(sampleListView);
                    listView.setAdapter(adapter);
                    listView.setLayoutAnimation(animation);
                    adapter.setClickListener(UserActivity.this);
                    refreshCollection.setRefreshing(false);
                }
            });
        }
    }


    public void refreshPlease() {
        sampleListView.clear();
        setUserView();
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            onFinishAcivity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_refresh:
                refreshPlease();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_settings) {
            MyStatic.setWhichactivity("SETTING");
            this.startActivityForResult(new Intent(this, NavigationActivity.class), 1);
        } else if (id == R.id.nav_send_feedback) {
            new EasyFeedback.Builder(this)
                    .withEmail("rhbussolutions@gmail.com")
                    .withSystemInfo()
                    .build()
                    .start();
        } else if (id == R.id.nav_grcard) {

            MyStatic.setWhichactivity("GOODSRECIEPT");
            this.startActivityForResult(new Intent(this, HomeViewActivity.class), 1);
        } else if (id == R.id.nav_pickcard) {

            MyStatic.setWhichactivity("PICKORDER");
            this.startActivityForResult(new Intent(this, HomeViewActivity.class), 1);
        } else if (id == R.id.nav_transfer) {

            MyStatic.setWhichactivity("TRANSFER");
            this.startActivityForResult(new Intent(this, HomeViewActivity.class), 1);
        } else if (id == R.id.nav_logout) {

            onFinishAcivity();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onFinishAcivity() {
        final SweetAlertDialog builder = new SweetAlertDialog(UserActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Are you sure ?")
                .setCustomImage(R.drawable.alertexit)
                .setContentText("Are you sure want to Logout");
        builder.setConfirmText("YES Exit!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        finish();
                    }
                });
        builder.setCancelText("CANCEL")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
        builder.show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected)
            showSnack(isConnected);
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            showSnack(isConnected);
            if (noInternetDialog != null ? !noInternetDialog.isShowing() : false) {
                noInternetDialog.show();
            }
        }
        return isConnected;
    }

    @Override
    public void onPause() {
        Bungee.zoom(this);
        MyApplication.getInstance().setConnectivityListener(this);

        super.onPause();
    }

    @Override
    protected void onResume() {
        Bungee.zoom(this);
        MyApplication.getInstance().setConnectivityListener(this);

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (noInternetDialog != null ? noInternetDialog.isShowing() : false)
            noInternetDialog.onDestroy();
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        if (isConnected) {
            MyApplication.displaySnackBar(UserActivity.this, MyApplication.SUCCESS, "Connection Established!");
        } else {
            MyApplication.displaySnackBar(UserActivity.this, MyApplication.ERROR, "Connection couldn't Established!\nCheck the Internet Connection");
        }
    }
}