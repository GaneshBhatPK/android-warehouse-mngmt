package com.probatus.rhbus.warehouse.Interface;

import java.util.Comparator;

/**
 * Created by Ganapathi on 11-03-2018.
 */

public class productversion {

    private String android_version_name,product_description,product_price;
    private String product_quantity,product_total,product_itemcode,product_barcode ;
    private String product_delete,tender_desc,tender_code;
    private String product_hsn,product_discount,product_uom,product_lineno;
    private String cgst_percent,cgst_amt,sgst_percent,sgst_amt;
    private String product_linedisc,product_isFOC;
    private String product_category,product_subcategory,product_brand;
    private byte[] android_image_url;
    private boolean SelectedFlag=false;

    public String getParameterNo_1() {
        return android_version_name;
    }

    public void setParameterNo_1(String android_version_name) { this.android_version_name = android_version_name; }

    public byte[] getAndroid_image_url() {
        return android_image_url;
    }

    public void setAndroid_image_url(byte[] android_image_url) { this.android_image_url = android_image_url; }

    public String getParameterNo_2() {
        return product_description;
    }

    public void setParameterNo_2(String product_description) { this.product_description = product_description; }

    public String getParameterNo_3() {
        return product_barcode;
    }

    public void setParameterNo_3(String product_barcode) { this.product_barcode = product_barcode; }

    public String getParameterNo_4() {
        return product_price;
    }

    public void setParameterNo_4(String product_price) { this.product_price = product_price; }

    public String getParameterNo_5() {
        return product_quantity;
    }

    public void setParameterNo_5(String product_quantity) { this.product_quantity = product_quantity; }

    public String getParameterNo_6() {
        return product_total;
    }

    public void setParameterNo_6(String product_total) {
        this.product_total = product_total;
    }

    public String getParameterNo_7() {
        return product_itemcode;
    }

    public void getParameterNo_7(String product_itemcode) { this.product_itemcode = product_itemcode; }

    public String getParameterNo_8() {
        return product_delete;
    }

    public void setParameterNo_8(String product_delete) {
        this.product_delete = product_delete;
    }

    public String getParameterNo_9() {
        return product_linedisc;
    }

    public void setParameterNo_9(String linedisc) {
        this.product_linedisc = linedisc;
    }

    public Boolean getSelectedFlag() { return SelectedFlag; }

    public void setSelectedFlag(Boolean context) {
        this.SelectedFlag = context;
    }

    public String getParameterNo_10() {
        return product_hsn;
    }

    public void setParameterNo_10(String product_hsn) {
        this.product_hsn = product_hsn;
    }

    public String getParameterNo_11() {
        return product_discount;
    }

    public void setParameterNo_11(String product_discount) { this.product_discount = product_discount;}

    public String getParameterNo_12() {
        return product_uom;
    }

    public void setParameterNo_12(String product_uom) {
        this.product_uom = product_uom;
    }

    public String getParameterNo_13() {
        return product_lineno;
    }

    public void setParameterNo_13(String product_lineno) {
        this.product_lineno = product_lineno;
    }

    public String getParameterNo_14() {
        return product_category;
    }

    public void setParameterNo_14(String product_category) { this.product_category = product_category; }

    public String getParameterNo_15() {
        return product_subcategory;
    }

    public void setParameterNo_15(String product_subcategory) { this.product_subcategory = product_subcategory; }

    public String getParameterNo_16() {
        return product_brand;
    }

    public void setParameterNo_16(String product_brand) {
        this.product_brand = product_brand;
    }

    public String getParameterNo_17() {
        return sgst_amt;
    }

    public void setParameterNo_17(String sgst_amt) {
        this.sgst_amt = sgst_amt;
    }



    public static Comparator<productversion> StuNameComparator = new Comparator<productversion>() {

        public int compare(productversion s1, productversion s2) {
            String StudentName1 = s1.getParameterNo_7().toUpperCase();
            String StudentName2 = s2.getParameterNo_7().toUpperCase();

            return StudentName1.compareTo(StudentName2);

        }};

    /*Comparator for sorting the list by roll no*/
    public static Comparator<productversion> StuDescComparator = new Comparator<productversion>() {

        public int compare(productversion s1, productversion s2) {
            String StudentName1 = s1.getParameterNo_2().toUpperCase();
            String StudentName2 = s2.getParameterNo_2().toUpperCase();

            return StudentName1.compareTo(StudentName2);

        }};

}
