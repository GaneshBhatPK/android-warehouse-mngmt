package com.probatus.rhbus.warehouse;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.hanks.htextview.HTextView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.probatus.rhbus.warehouse.Adapter.ListAdapter;
import com.probatus.rhbus.warehouse.Interface.AdapterCallback;
import com.probatus.rhbus.warehouse.Interface.ExceptionHandler;
import com.probatus.rhbus.warehouse.Interface.productversion;
import com.probatus.rhbus.warehouse.JsonParse.HttpResponse;
import com.probatus.rhbus.warehouse.JsonParse.JsonResponse;
import com.probatus.rhbus.warehouse.JsonParse.MyStatic;
import com.viethoa.RecyclerViewFastScroller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import am.appwise.components.ni.NoInternetDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import in.evrencoskun.tableviewsample.tableview.TableViewAdapter;
import in.evrencoskun.tableviewsample.tableview.TableViewListener;
import in.evrencoskun.tableviewsample.tableview.TableViewModel;
import in.evrencoskun.tableviewsample.tableview.model.Cell;
import in.evrencoskun.tableviewsample.tableview.model.ColumnHeader;
import in.evrencoskun.tableviewsample.tableview.model.RowHeader;
import lecho.cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;
import spencerstudios.com.bungeelib.Bungee;

/**
 * Created by ganapathi on 13/1/20.
 */

public class WMSPickCard extends AppCompatActivity implements
        ConnectivityReceiver.ConnectivityReceiverListener, AdapterCallback {

    JsonResponse jsonResponse = new JsonResponse();
    RecyclerView lv;
    SwipeRefreshLayout refreshCollection;
    RecyclerViewFastScroller recyclerViewFastScroller;
    RecyclerView.SmoothScroller smoothScroller;
    FancyButton list_void;
    LayoutAnimationController animation;
    ListAdapter arrayAdapter;
    JSONArray dataSourcePICKADHQuantityToPick = new JSONArray();
    JSONArray dataHeader = new JSONArray();
    JSONArray dataLines = new JSONArray();

    private static final int REQUEST_ENABLE_BT = 1;

    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    ArrayList<BluetoothDevice> pairedDeviceArrayList;
    ArrayAdapter<BluetoothDevice> pairedDeviceAdapter;
    private UUID myUUID;
    private final String UUID_STRING_WELL_KNOWN_SPP =
            "00001101-0000-1000-8000-00805F9B34FB";

    WMSPickCard.ThreadConnectBTdevice myThreadConnectBTdevice;
    WMSPickCard.ThreadConnected myThreadConnected;
    TextView textInfo, txtDocument, txtCustomer, txtCustVendLabel, list_voidtxt;
    HTextView textStatus;
    ListView listViewPairedDevice;
    LinearLayout inputPane;
    FancyButton btnSend, btnCancel, alertPopup;
    Dialog dialog;
    Integer indexOfList = null;
    Boolean isAddMore = false, isShowAll = false;
    String msgReceived = "";
    MaterialSearchView searchView;
    public productversion productversionsList;
    public static ArrayList<productversion> productversions = new ArrayList<>();
    public ArrayList<productversion> selected_items = new ArrayList<>();
    ToneGenerator toneGen1;
    NoInternetDialog noInternetDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyStatic.setmContext(this);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        setContentView(R.layout.activity_listcontent);
        toneGen1 = new ToneGenerator(AudioManager.STREAM_SYSTEM, 100);

        refreshCollection = (SwipeRefreshLayout) findViewById(R.id.listswipeRefreshLayout);
        lv = (RecyclerView) findViewById(R.id.list_view);
        recyclerViewFastScroller = (RecyclerViewFastScroller) findViewById(R.id.fast_scroller);
        list_void = (FancyButton) findViewById(R.id.list_void);
        list_voidtxt = (TextView) findViewById(R.id.list_voidtxt);
        txtDocument = (TextView) findViewById(R.id.txtDocument);
        txtCustomer = (TextView) findViewById(R.id.txtCustomer);
        txtCustVendLabel = (TextView) findViewById(R.id.txtCustVendlabel);
        txtCustVendLabel.setText("Customer");
        recyclerViewFastScroller.setVisibility(View.GONE);

        animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        smoothScroller = new LinearSmoothScroller(this) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        getHeaderDetails();
        refreshCollection.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                indexOfList = null;
                if (isShowAll) {
                    setListforShowAllView();
                } else {
                    setListforView();
                    lv.setLayoutAnimation(animation);
                }
            }
        });

        list_void.setIconResource(R.drawable.ic_full_showall);
        list_voidtxt.setText("SHOW ALL");
        list_void.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShowAll = !isShowAll;
                indexOfList = null;
                if (isShowAll) {
                    setListforShowAllView();
                    list_void.setIconResource(R.drawable.ic_full_pickall);
                    list_voidtxt.setText("PICK ALL");
                } else {
                    setListforView();
                    list_void.setIconResource(R.drawable.ic_full_showall);
                    list_voidtxt.setText("SHOW ALL");
                }
            }
        });

        dialog = new Dialog(WMSPickCard.this);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setTitle("Bluetooth Setup");
        dialog.setContentView(R.layout.activity_scalereader);
        textInfo = (TextView) dialog.findViewById(R.id.info);
        textStatus = (HTextView) dialog.findViewById(R.id.status);
        listViewPairedDevice = (ListView) dialog.findViewById(R.id.pairedlist);

        inputPane = (LinearLayout) dialog.findViewById(R.id.inputpane);
        btnSend = (FancyButton) dialog.findViewById(R.id.send);
        btnCancel = (FancyButton) dialog.findViewById(R.id.cancel);
        alertPopup = (FancyButton) dialog.findViewById(R.id.alertPopup);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Double inputQty = Double.parseDouble(msgReceived);
                    JSONObject jsonObject = dataSourcePICKADHQuantityToPick.getJSONObject(indexOfList);
                    if (isAddMore) {
                        inputQty = inputQty + jsonObject.getDouble("QuantityToPick");
                    }
                    if (jsonObject.getDouble("Available") < inputQty) {
                        msgReceived = jsonObject.getString("Available");
                        inputQty = Double.parseDouble(msgReceived);
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Qty To Pick is Exceeds Available Quantity, So it has been reseted!!");
                    }
                    jsonObject.remove("QuantityToPick");
                    jsonObject.remove("ReCalculatedQty");
                    jsonObject.put("QuantityToPick", inputQty);
                    if (jsonObject.getDouble("reqQty") < inputQty) {
                        msgReceived = "0.00";
                        inputQty = Double.parseDouble(msgReceived);
                    } else {
                        msgReceived = Double.toString(roundTwoDecimals(jsonObject.getDouble("reqQty") - inputQty));
                        inputQty = Double.parseDouble(msgReceived);
                    }
                    jsonObject.put("ReCalculatedQty", inputQty);
                    for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                        JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                        if (currObject.getString("LineNo").equals(jsonObject.getString("LineNo")) &&
                                currObject.getString("LotLineNo").equals(jsonObject.getString("LotLineNo"))) {
                            currObject.remove("ReCalculatedQty");
                            currObject.put("ReCalculatedQty", inputQty);
                            dataSourcePICKADHQuantityToPick.put(i, currObject);
                        }
                    }
                    if (dataSourcePICKADHQuantityToPick.length() > indexOfList) {
                        dataSourcePICKADHQuantityToPick.put(indexOfList, jsonObject);
                    } else
                        dataSourcePICKADHQuantityToPick.put(jsonObject);
                    chechDataBeforeRequest(indexOfList);
                } catch (NumberFormatException e) {
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Not a Valid Number!");
                } catch (Exception e) {
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + e);
                }
                if (dialog.isShowing())
                    dialog.dismiss();
                resetBluetoothConn(true);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing())
                    dialog.dismiss();
                resetBluetoothConn(true);
            }
        });
        alertPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog.isShowing())
                    dialog.dismiss();
                applyQuantity(indexOfList);
            }
        });

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "FEATURE_BLUETOOTH NOT SUPPORT");
            finish();
            return;
        }

        //using the well-known SPP UUID
        myUUID = UUID.fromString(UUID_STRING_WELL_KNOWN_SPP);
        if (bluetoothAdapter == null) {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Bluetooth is not supported on this hardware platform");
            finish();
            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(true);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (arrayAdapter != null)
                    arrayAdapter.filter(query, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (arrayAdapter != null)
                    arrayAdapter.filter(newText, 1);
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        noInternetDialog = new NoInternetDialog.Builder(this).build();
    }

    public void chechDataBeforeRequest(int j) {
        switch (checkData(j)) {
            case 1: {
                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Required OR Available Qty = 0, NO_REQUIRED OR NO_STOCK_AVAILABLE");
                setListforView();
                break;
            }
            case 2: {
                SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("Confirmation")
                        .setCustomImage(R.drawable.alertcart)
                        .setContentText("This Quantity has been EXCEEDS!");
                builder.setConfirmText("Yes, Proceed")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                sendSingleRequest(j);
                            }
                        });
                builder.setCancelText("No, Edit")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                setListforView();
                            }
                        });
                builder.show();

                break;
            }
            default: {
                sendSingleRequest(j);
                break;
            }
        }
    }

    public void sendSingleRequest(int previous) {
        SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this,
                SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        builder.setCancelable(false);
        builder.show();

        try {
            JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(previous);
            JSONObject dataH = dataHeader.getJSONObject(0);

            ArrayList<String> list = new ArrayList<String>();
            list.add(MyStatic.getTokenKey());
            list.add(MyApplication.getInstance().getDocumentNumber());
            list.add(currObject.getString("LineNo"));
            list.add(currObject.getString("QuantityToPick"));
            list.add(currObject.getString("SUCode"));
            list.add(MyApplication.getInstance().getSomeVariable().get(0).toString());
            list.add(dataH.getString("DocumentDate"));
            list.add(dataH.getString("LocationCode"));
            jsonResponse.JsonResponse(WMSPickCard.this, "wmsPickCard_android", "button1_clickHandlerbyNoLot", list, new HttpResponse<JSONObject>() {
                public void onResponse(JSONObject response) throws JSONException {
                    if (builder.isShowing()) builder.dismissWithAnimation();
                    try {
                        JSONArray array = response.getJSONArray("result");
                        if (array.get(0).toString().equals("null") || array.get(0) == null) {
                            setListViewFromData(array.getJSONArray(2));
                            MyApplication.displaySnackBar(WMSPickCard.this,MyApplication.WARNING, "தயவுசெய்து 4-6 விநாடிகள் காத்திருக்கவும், மல்டி பிக் தோல்வியுற்றது");
                        } else if (array.get(0).toString().equals("DONE")) {
                            setListViewFromData(array.getJSONArray(2));
                            //MyApplication.displaySnackBar(WMSPickCard.this,MyApplication.SUCCESS, "Successfully Update the Move to Ship Zones");
                        } else if (array.get(0).toString().equals("TOKEN_EXPIRY")) {
                            alertLoginPopup();
                        } else {
                            setListViewFromData(array.getJSONArray(2));
                            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Failed to Update the Move to Ship Zones: " + array.get(0).toString());
                        }
                    } catch (Exception e) {
                        setListforView();
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                    }
                }
            });
        } catch (Exception e) {
            if (builder.isShowing()) builder.dismissWithAnimation();
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + e);
        }
    }

    public void sendBulkRequest() {
        SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this,
                SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        builder.setCancelable(false);
        builder.show();

        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        list.add(MyApplication.getInstance().getSomeVariable().get(0).toString());
        list.add(dataSourcePICKADHQuantityToPick.toString());
        jsonResponse.JsonResponse(WMSPickCard.this, "wmsPickCard_android", "wmspicklot", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                if (builder.isShowing()) builder.dismissWithAnimation();
                try {
                    String array = response.getString("status");
                    if (array.equals("DONE")) {
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.SUCCESS, "Successfully Update the Move to Ship Zones");
                        MyStatic.setWhichactivity("PICKORDER");
                        Intent tender = new Intent(WMSPickCard.this, HomeViewActivity.class);
                        startActivity(tender);
                        finish();
                    } else if (array.equals("TOKEN_EXPIRY")) {
                        alertLoginPopup();
                    } else {
                        setListforView();
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Failed to Update the Move to Ship Zones: " + array);
                    }
                } catch (Exception e) {
                    setListforView();
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
            }
        });
    }

    public void alertLoginPopup() {
        SweetAlertDialog popuplogin = new SweetAlertDialog(this,
                SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Login")
                .setContentText("Login with User Name :" + MyApplication.getInstance().getSomeVariable().get(0))
                .setConfirmText("PROCEED UPLOAD")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        ArrayList<String> list = new ArrayList<String>();
                        list.add(MyApplication.getInstance().getSomeVariable().get(2).toString());
                        list.add(MyApplication.getInstance().getSomeVariable().get(0).toString());
                        list.add(MyApplication.getInstance().getSomeVariable().get(1).toString());
                        jsonResponse.JsonResponse(WMSPickCard.this, "Auth3", "inCreds", list,
                                new HttpResponse<JSONObject>() {
                                    public void onResponse(JSONObject response) {
                                        try {
                                            JSONObject jsonObj = response.getJSONArray("data").getJSONObject(0);
                                            MyStatic.setTokenKey(jsonObj.getString("accesstoken"));
                                            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.INFO, "Token Regenerated, Now Try Again!!");
                                            if (isShowAll) {
                                                setListforShowAllView();
                                            } else {
                                                setListforView();
                                            }
                                        } catch (JSONException e) {
                                        }
                                    }
                                });
                    }
                });
        popuplogin.setCancelable(false);
        popuplogin.show();
    }

    public void setBletooth() {
        String stInfo = bluetoothAdapter.getName() + " " + bluetoothAdapter.getAddress();
        if (textInfo != null) textInfo.setText(stInfo);

        if (!dialog.isShowing())
            dialog.show();
        if (myThreadConnectBTdevice != null ? myThreadConnectBTdevice.bluetoothSocket != null ? myThreadConnected != null : false : false) {
            if (myThreadConnectBTdevice.bluetoothSocket.isConnected()) {
                msgReceived = "0.00";
                textStatus.animateText("");
                textInfo.setText(myThreadConnectBTdevice.bluetoothDevice.getName());
                myThreadConnected.displayValue = "Establishing Connection..";
                listViewPairedDevice.setVisibility(View.GONE);
                inputPane.setVisibility(View.VISIBLE);
                textStatus.animateText(msgReceived);
                dialog.setCancelable(false);
                if (isAddMore) {
                    alertPopup.setVisibility(View.VISIBLE);
                } else {
                    alertPopup.setVisibility(View.GONE);
                }
            } else {
                setup();
                textStatus.reset("");
                msgReceived = "Establishing Connection..";
                listViewPairedDevice.setVisibility(View.VISIBLE);
                inputPane.setVisibility(View.GONE);
                textStatus.animateText(msgReceived);
                if (isAddMore) {
                    alertPopup.setVisibility(View.VISIBLE);
                } else {
                    alertPopup.setVisibility(View.GONE);
                }
            }
        } else {
            setup();
            textStatus.reset("");
            msgReceived = "Establishing Connection..";
            listViewPairedDevice.setVisibility(View.VISIBLE);
            inputPane.setVisibility(View.GONE);
            textStatus.animateText(msgReceived);
            if (isAddMore) {
                alertPopup.setVisibility(View.VISIBLE);
            } else {
                alertPopup.setVisibility(View.GONE);
            }
        }
    }

    public String isNullChecker(String userEmail) {
        if (userEmail != null && (!TextUtils.equals(userEmail, "null")) && (!TextUtils.isEmpty(userEmail))) {
            return userEmail;
        } else {
            return "-";
        }
    }

    private void getHeaderDetails() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        jsonResponse.JsonResponse(this, "wmsPickCard", "openPickOrder", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                try {
                    JSONArray array = response.getJSONArray("result");
                    dataHeader = array;
                    String phone;
                    try {
                        phone = " Ph: " + isNullChecker(array.getJSONObject(0).getString("ShiptoPhone"));
                    } catch (Exception e) {
                        phone = "";
                    }
                    txtDocument.setText(MyApplication.getInstance().getDocumentNumber());
                    txtCustomer.setText(isNullChecker(array.getJSONObject(0).getString("ShiptoName")) + phone);
                    if (dataHeader.length() > 0) {
                        setListforView();
                    }
                } catch (Exception e) {
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
            }
        });
    }

    private void getLinesDetails() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        jsonResponse.JsonResponse(this, "wmsPickCard", "openPickLines", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                try {
                    JSONArray array = response.getJSONArray("result");
                    dataLines = array;
                    onClickLinesDeatils();
                } catch (Exception e) {
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
            }
        });
    }

    private void setting() {
        /* ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < productversions.size(); i++) {
            String name = productversions.get(i).getParameterNo_1();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word.toLowerCase(Locale.getDefault())) &&
                    !strAlphabets.contains(word.toUpperCase(Locale.getDefault()))) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
        recyclerViewFastScroller.setRecyclerView(lv);
        recyclerViewFastScroller.setUpAlphabet(mAlphabetItems); */
        refreshCollection.setRefreshing(false);
    }

    private void setListViewFromData(JSONArray array) {
        try {
            selected_items = new ArrayList<>();
            if (array.length() == 0) {
                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "No Lines with LotNumber!!");
            }
            dataSourcePICKADHQuantityToPick = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject c = array.getJSONObject(i);
                c.put("QuantityToPick", 0);
                c.put("ReCalculatedQty", c.getString("reqQty"));
                if (c.getString("LineNo") == null) {
                    c.remove("LineNo");
                    c.put("LineNo", i);
                }
                c.put("LotLineNo", i);
                dataSourcePICKADHQuantityToPick.put(c);

                productversionsList = new productversion();
                productversionsList.setParameterNo_1(c.getString("Description"));
                productversionsList.setParameterNo_2(c.getString("Description"));
                productversionsList.setParameterNo_3("" + i);
                productversionsList.setParameterNo_4(c.getString("ItemCode"));
                productversionsList.setParameterNo_6(c.getString("ItemCode") + "( " + c.getString("Available") + " )");
                productversionsList.getParameterNo_7(c.getString("ItemCode"));

                productversionsList.setParameterNo_5(c.getString("QuantityToPick"));
                productversionsList.setParameterNo_8(c.getString("ReCalculatedQty"));
                if (c.getDouble("QuantityToPick") >= c.getDouble("reqQty")) {
                    productversionsList.setParameterNo_11("EQUAL");
                } else if (c.getDouble("reqQty") != c.getDouble("QtyinSOUOM")) {
                    productversionsList.setParameterNo_11("LESS");
                } else {
                    productversionsList.setParameterNo_11("ZERO");
                }

                productversionsList.setParameterNo_9(c.getString("Available"));
                productversionsList.setParameterNo_10(c.getString("BaseUOM"));
                productversionsList.setParameterNo_12(c.getString("ItemCode") + " " +
                        c.getString("Description"));
                productversionsList.setParameterNo_13(c.getString("LineNo"));
                productversionsList.setParameterNo_14(c.getString("ItemCode"));
                productversionsList.setParameterNo_15(c.getString("ItemCode"));
                productversionsList.setParameterNo_16(c.getString("SUCode"));
                productversionsList.setParameterNo_17(c.getString("reqQty"));
                selected_items.add(productversionsList);
            }
        } catch (Exception e) {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + e);
        }
        productversions = selected_items;
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(WMSPickCard.this, 1);
        lv.setLayoutManager(layoutManager);
        arrayAdapter = new ListAdapter(WMSPickCard.this, productversions);
        lv.setAdapter(arrayAdapter);
        if (indexOfList != null) {
            if (indexOfList >= productversions.size()) {
                indexOfList = productversions.size() - 1;
            }
            lv.scrollToPosition(indexOfList);
        }
        setting();
    }

    private void setListforView() {
        refreshCollection.setRefreshing(true);

        try {
            JSONObject dataH = dataHeader.getJSONObject(0);
            ArrayList<String> list = new ArrayList<String>();
            list.add(MyStatic.getTokenKey());
            list.add(MyApplication.getInstance().getDocumentNumber());
            list.add(dataH.getString("DocumentDate"));
            list.add(dataH.getString("LocationCode"));
            jsonResponse.JsonResponse(WMSPickCard.this, "wmsPickCard_android", "getpickadhoclinesbyNoLot", list, new HttpResponse<JSONObject>() {
                public void onResponse(JSONObject response) throws JSONException {
                    selected_items = new ArrayList<>();
                    try {
                        JSONArray array = response.getJSONArray("result");
                        if (array.getString(0).equals("DONE")) {
                            array = array.getJSONArray(1);
                            if (array.length() == 0) {
                                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "No Lines with LotNumber!!");
                            }
                            dataSourcePICKADHQuantityToPick = new JSONArray();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject c = array.getJSONObject(i);
                                c.put("QuantityToPick", 0);
                                c.put("ReCalculatedQty", c.getString("reqQty"));
                                if (c.getString("LineNo") == null) {
                                    c.remove("LineNo");
                                    c.put("LineNo", i);
                                }
                                c.put("LotLineNo", i);
                                dataSourcePICKADHQuantityToPick.put(c);

                                productversionsList = new productversion();
                                productversionsList.setParameterNo_1(c.getString("Description"));
                                productversionsList.setParameterNo_2(c.getString("Description"));
                                productversionsList.setParameterNo_3("" + i);
                                productversionsList.setParameterNo_4(c.getString("ItemCode"));
                                productversionsList.setParameterNo_6(c.getString("ItemCode") + "( " + c.getString("Available") + " )");
                                productversionsList.getParameterNo_7(c.getString("ItemCode"));

                                productversionsList.setParameterNo_5(c.getString("QuantityToPick"));
                                productversionsList.setParameterNo_8(c.getString("ReCalculatedQty"));
                                if (c.getDouble("QuantityToPick") >= c.getDouble("reqQty")) {
                                    productversionsList.setParameterNo_11("EQUAL");
                                } else if (c.getDouble("reqQty") != c.getDouble("QtyinSOUOM")) {
                                    productversionsList.setParameterNo_11("LESS");
                                } else {
                                    productversionsList.setParameterNo_11("ZERO");
                                }

                                productversionsList.setParameterNo_9(c.getString("Available"));
                                productversionsList.setParameterNo_10(c.getString("BaseUOM"));
                                productversionsList.setParameterNo_12(c.getString("ItemCode") + " " +
                                        c.getString("Description"));
                                productversionsList.setParameterNo_13(c.getString("LineNo"));
                                productversionsList.setParameterNo_14(c.getString("ItemCode"));
                                productversionsList.setParameterNo_15(c.getString("ItemCode"));
                                productversionsList.setParameterNo_16(c.getString("SUCode"));
                                productversionsList.setParameterNo_17(c.getString("reqQty"));
                                selected_items.add(productversionsList);
                            }
                        } else {
                            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Error While fetching Lines :" + array.getString(0));
                        }
                    } catch (Exception e) {
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                    }
                    productversions = selected_items;
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(WMSPickCard.this, 1);
                    lv.setLayoutManager(layoutManager);
                    arrayAdapter = new ListAdapter(WMSPickCard.this, productversions);
                    lv.setAdapter(arrayAdapter);
                    if (indexOfList != null) {
                        if (indexOfList >= productversions.size()) {
                            indexOfList = productversions.size() - 1;
                        }
                        lv.scrollToPosition(indexOfList);
                    }
                    setting();
                }
            });
        } catch (JSONException e) {
            refreshCollection.setRefreshing(false);
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + e);
        }
    }

    private void setListforShowAllView() {
        refreshCollection.setRefreshing(true);

        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        jsonResponse.JsonResponse(this, "wmsPickCard", "openPickLines", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                selected_items = new ArrayList<>();
                try {
                    JSONArray array = response.getJSONArray("result");
                    if (array.length() == 0) {
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "No Lines Present with Pick Number !!");
                    }
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        productversionsList = new productversion();
                        productversionsList.setParameterNo_1(c.getString("Description"));
                        productversionsList.setParameterNo_2(c.getString("Description"));
                        productversionsList.setParameterNo_3(c.getString("ItemCode"));
                        productversionsList.setParameterNo_4(c.getString("SOUOM"));
                        productversionsList.setParameterNo_5(c.getString("ShippingQty"));
                        productversionsList.setParameterNo_6(c.getString("ItemCode"));
                        productversionsList.getParameterNo_7(c.getString("ItemCode"));
                        productversionsList.setParameterNo_8(c.getString("QtyinSOUOM"));
                        productversionsList.setParameterNo_9("1.00");
                        productversionsList.setParameterNo_10(c.getString("BaseUOM"));
                        productversionsList.setParameterNo_11("SHOWALLLINES");
                        productversionsList.setParameterNo_12(c.getString("ItemCode") + " " +
                                c.getString("Description"));
                        productversionsList.setParameterNo_13(c.getString("LineNo"));
                        productversionsList.setParameterNo_14(c.getString("ShippedQty"));
                        productversionsList.setParameterNo_15(c.getString("SOUnitPrice"));
                        productversionsList.setParameterNo_16(c.getString("ItemCode"));
                        productversionsList.setParameterNo_17(c.getString("ShippedQty"));
                        selected_items.add(productversionsList);
                    }
                } catch (Exception e) {
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
                productversions = selected_items;
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(WMSPickCard.this, 1);
                lv.setLayoutManager(layoutManager);
                arrayAdapter = new ListAdapter(WMSPickCard.this, productversions);
                lv.setAdapter(arrayAdapter);
                lv.setLayoutAnimation(animation);
                setting();
            }
        });
    }

    public int checkData(int previous) {
        try {
            JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(previous);
            Double recQty = Double.valueOf(currObject.getString("QuantityToPick"));
            Double avb = Double.valueOf(currObject.getString("Available"));
            Double reqQty = Double.valueOf(currObject.getString("reqQty"));
            if (recQty == 0 || avb == 0) {
                // reset the entered input
                return 1;
            } else if ((recQty > avb) || (recQty > reqQty)) {
                return 2;
            }
            return 3;
        } catch (Exception e) {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + e);
            return 1;
        }
    }

    public void onMethodCallback(final String ls, final String qnty, final TextView ttl, View v, String status, final int j) {
        if (status == "qt") {
            isAddMore = false;
            try {
                for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                    JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                    if (currObject.getString("LineNo").equals(ls) &&
                            currObject.getString("LotLineNo").equals(qnty)) {
                        indexOfList = i;
                        applyQuantity(i);
                        break;
                    }
                }
            } catch (Exception e) {
            }
        } else if (status == "weight") {
            isAddMore = false;
            try {
                for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                    JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                    if (currObject.getString("LineNo").equals(ls) &&
                            currObject.getString("LotLineNo").equals(qnty)) {
                        indexOfList = i;
                        setBletooth();
                        break;
                    }
                }
            } catch (Exception e) {
            }
        } else if (status == "addmore") {
            isAddMore = true;
            try {
                for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                    JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                    if (currObject.getString("LineNo").equals(ls) &&
                            currObject.getString("LotLineNo").equals(qnty)) {
                        indexOfList = i;
                        setBletooth();
                        break;
                    }
                }
            } catch (Exception e) {
            }
        } else if (status == "redo") {
            isAddMore = false;
            final SweetAlertDialog builder = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setTitleText("Are you Sure ?")
                    .setCustomImage(R.drawable.alertvoid)
                    .setContentText("You Cannot Roll back Once you CLEAR !")
                    .setConfirmText("YES! CLEAR")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            if (isShowAll) {
                                try {
                                    for (int i = 0; i < selected_items.size(); i++) {
                                        if (selected_items.get(i).getParameterNo_13().equals(ls) &&
                                                selected_items.get(i).getParameterNo_3().equals(qnty)) {
                                            indexOfList = i;
                                            clearAlltheWeights(i);
                                            break;
                                        }
                                    }
                                } catch (Exception e) {
                                }
                            } else {
                                try {
                                    for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                                        JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                                        if (currObject.getString("LineNo").equals(ls) &&
                                                currObject.getString("LotLineNo").equals(qnty)) {
                                            indexOfList = i;
                                            clearAlltheWeights(i);
                                            break;
                                        }
                                    }
                                } catch (Exception e) {
                                }
                            }
                        }
                    });
            builder.setCancelText("CANCEL")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
            builder.show();
        } else if (status == "equal") {
            isAddMore = false;
            try {
                for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                    JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                    if (currObject.getString("LineNo").equals(ls) &&
                            currObject.getString("LotLineNo").equals(qnty)) {
                        indexOfList = i;
                        equalAlltheWeights(i);
                        break;
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public void applyQuantity(final int j) {
        final SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Quantity")
                .setCustomImage(R.drawable.alertquantity)
                .setContentText("Enter the Quantity :");
        final EditText input = new EditText(WMSPickCard.this);
        input.setHint("Enter The Quantity");
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        builder.setCustomView(input);
        builder.setConfirmText("Apply")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        try {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.RESULT_HIDDEN, 0);
                        } catch (Exception e) {
                        }
                        if (!TextUtils.isEmpty(input.getText().toString())
                                && !input.getText().toString().equals("0")) {
                            try {
                                Double inputQty = Double.parseDouble(input.getText().toString());
                                JSONObject jsonObject = dataSourcePICKADHQuantityToPick.getJSONObject(j);
                                if (isAddMore) {
                                    inputQty = inputQty + jsonObject.getDouble("QuantityToPick");
                                }
                                if (jsonObject.getDouble("Available") < inputQty) {
                                    inputQty = jsonObject.getDouble("Available");
                                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Qty To Pick is Exceeds Available Quantity, So it has been reseted!!");
                                }
                                jsonObject.remove("QuantityToPick");
                                jsonObject.remove("ReCalculatedQty");
                                jsonObject.put("QuantityToPick", inputQty.toString());
                                if (jsonObject.getDouble("reqQty") < inputQty) {
                                    inputQty = 0.00;
                                } else {
                                    inputQty = roundTwoDecimals(jsonObject.getDouble("reqQty") - inputQty);
                                }
                                jsonObject.put("ReCalculatedQty", inputQty.toString());
                                for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                                    JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                                    if (currObject.getString("LineNo").equals(jsonObject.getString("LineNo")) &&
                                            currObject.getString("LotLineNo").equals(jsonObject.getString("LotLineNo"))) {
                                        currObject.remove("ReCalculatedQty");
                                        currObject.put("ReCalculatedQty", inputQty.toString());
                                        dataSourcePICKADHQuantityToPick.put(i, currObject);
                                    }
                                }
                                if (dataSourcePICKADHQuantityToPick.length() > j) {
                                    dataSourcePICKADHQuantityToPick.put(j, jsonObject);
                                } else
                                    dataSourcePICKADHQuantityToPick.put(jsonObject);
                                chechDataBeforeRequest(j);
                            } catch (NumberFormatException e) {
                                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Not a Valid Number!");
                            } catch (Exception e) {
                                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "" + e);
                            }
                        } else {
                            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "No Quantity Given!!");
                        }
                    }
                });
        builder.setCancelText("CANCEL")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
        builder.show();
    }

    public void clearAlltheWeights(int previous) {
        SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this,
                SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        builder.setCancelable(false);
        builder.show();

        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        list.add(selected_items.get(previous).getParameterNo_13());
        jsonResponse.JsonResponse(WMSPickCard.this, "wmsPickCard_android", "buttonUndo_clickHandler", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                if (builder.isShowing()) builder.dismissWithAnimation();

                try {
                    JSONArray array = response.getJSONArray("result");
                    if (array.get(0).toString().equals("DONE")) {
                        if (isShowAll) {
                            setListforShowAllView();
                        } else {
                            setListforView();
                        }
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.SUCCESS, "Successfully Cleared the Picks!!");
                    } else if (array.get(0).toString().equals("TOKEN_EXPIRY")) {
                        alertLoginPopup();
                    } else {
                        if (isShowAll) {
                            setListforShowAllView();
                        } else {
                            setListforView();
                        }
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Failed to Update the Move to Ship Zones: " + array.get(0).toString());
                    }
                } catch (Exception e) {
                    if (isShowAll) {
                        setListforShowAllView();
                    } else {
                        setListforView();
                    }
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
            }
        });
    }

    public void undoAlltheWeights() {
        SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this,
                SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText("Loading");
        builder.setCancelable(false);
        builder.show();

        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        list.add(MyApplication.getInstance().getDocumentNumber());
        list.add(MyApplication.getInstance().getSomeVariable().get(0).toString());
        jsonResponse.JsonResponse(WMSPickCard.this, "wmsPickCard_android", "btnUndo_clickHandler", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) throws JSONException {
                if (builder.isShowing()) builder.dismissWithAnimation();
                try {
                    JSONArray array = response.getJSONArray("result");
                    if (array.get(0).toString().equals("DONE")) {
                        if (isShowAll) {
                            setListforShowAllView();
                        } else {
                            setListforView();
                        }
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.SUCCESS, "Successfully Cleared ALL the Picks!!");
                    } else if (array.get(0).toString().equals("TOKEN_EXPIRY")) {
                        alertLoginPopup();
                    } else {
                        if (isShowAll) {
                            setListforShowAllView();
                        } else {
                            setListforView();
                        }
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Failed to Undo the Move to Ship Zones: " + array.get(0).toString());
                    }
                } catch (JSONException e) {
                    if (isShowAll) {
                        setListforShowAllView();
                    } else {
                        setListforView();
                    }
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "" + response.getString("result"));
                }
            }
        });
    }

    public void equalAlltheWeights(int j) {
        try {
            JSONObject jsonObject = dataSourcePICKADHQuantityToPick.getJSONObject(j);
            for (int i = 0; i < dataSourcePICKADHQuantityToPick.length(); i++) {
                JSONObject currObject = dataSourcePICKADHQuantityToPick.getJSONObject(i);
                if (currObject.getString("LineNo").equals(jsonObject.getString("LineNo")) &&
                        currObject.getString("LotLineNo").equals(jsonObject.getString("LotLineNo"))) {
                    double inputQty = currObject.getDouble("reqQty");
                    if (jsonObject.getDouble("Available") < currObject.getDouble("reqQty")) {
                        inputQty = jsonObject.getDouble("Available");
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "Qty To Pick is Exceeds Available Quantity, So it has been reseted!!");
                    }
                    currObject.remove("ReCalculatedQty");
                    currObject.put("ReCalculatedQty", "" + (currObject.getDouble("reqQty") - inputQty));
                    currObject.remove("QuantityToPick");
                    currObject.put("QuantityToPick", inputQty);
                    dataSourcePICKADHQuantityToPick.put(i, currObject);
                    chechDataBeforeRequest(i);
                    break;
                }
            }
        } catch (Exception e) {
        }
    }

    public void onClickHeaderDeatils() {
        final SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Header")
                .setCustomImage(R.drawable.alerttxtreport);
        View dialogView = this.getLayoutInflater().inflate(R.layout.doc_headerdetails, null);
        TableLayout tableBrand = dialogView.findViewById(R.id.table);
        tableBrand.removeAllViews();
        String[] lineHeader = {"DocumentNo", "DocumentDate", "LocationCode", "DefPickStorage",
                "SourceNo", "Remarks", "RemarksExternal", "SOShipmentDate", "SelltoCustomerNo",
                "ShiptoCode", "ShiptoName", "ShiptoAddress", "ShiptoAddress2",
                "ShiptoCity", "ShiptoPostCode"};
        try {
            for (int i = 0; i < lineHeader.length; i++) {
                TableRow header = new TableRow(this);
                TextView tableHeader = new TextView(this);
                tableHeader.setTextColor(Color.BLACK);
                tableHeader.setTypeface(null, Typeface.BOLD);
                tableHeader.setGravity(Gravity.RIGHT);
                tableHeader.setText("  " + isNullChecker(lineHeader[i]) + "  ");
                tableHeader.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fade_in));
                tableHeader.setBackground(getResources().getDrawable(R.drawable.cell_header));
                header.addView(tableHeader);

                JSONObject jsonObject = dataHeader.getJSONObject(0);
                tableHeader = new TextView(this);
                tableHeader.setTextColor(Color.BLACK);
                tableHeader.setGravity(Gravity.RIGHT);
                tableHeader.setText("  " + isNullChecker(jsonObject.get(lineHeader[i]).toString()) + "  ");
                tableHeader.setBackground(getResources().getDrawable(R.drawable.cell_header));
                tableHeader.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.fade_in));
                header.addView(tableHeader);

                tableBrand.addView(header);
            }
        } catch (JSONException e) {
        } catch (Exception e) {
        }
        builder.setCustomView(dialogView);
        builder.setConfirmButton("CANCEL", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        builder.show();
    }

    public void onClickLinesDeatils() {
        final SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Lines")
                .setCustomImage(R.drawable.alerttxtreport);
        View dialogView = this.getLayoutInflater().inflate(R.layout.doc_linesdetails, null);
        TableViewModel tableViewModel = new TableViewModel();
        TableViewAdapter tableViewAdapter = new TableViewAdapter(tableViewModel);

        TableView mTableView = dialogView.findViewById(R.id.content_container);
        mTableView.setAdapter(tableViewAdapter);
        mTableView.setTableViewListener(new TableViewListener(mTableView));
        mTableView.setHasFixedWidth(false);
        String[] lineHeader = {"ItemCode", "Barcode", "BaseUOM", "QtyinBaseUOM",
                "SOUOM", "QtyinSOUOM", "ShippingQty", "ShippedQty"};

        List<ColumnHeader> headerlist = new ArrayList<>();
        List<RowHeader> rowHeaderlist = new ArrayList<>();
        List<List<Cell>> celllist = new ArrayList<>();

        try {
            for (int i = 0; i < lineHeader.length; i++) {
                ColumnHeader header = new ColumnHeader(String.valueOf(i), lineHeader[i]);
                headerlist.add(header);
            }
            for (int i = 0; i < dataLines.length(); i++) {
                RowHeader rowHeader = new RowHeader(String.valueOf(i), String.valueOf(i + 1));
                rowHeaderlist.add(rowHeader);

                List<Cell> childcellList = new ArrayList<>();
                JSONObject jsonObject = dataLines.getJSONObject(i);
                for (int j = 0; j < lineHeader.length; j++) {
                    Cell cell = new Cell(String.valueOf(i + j), isNullChecker(jsonObject.getString(lineHeader[j])));
                    childcellList.add(cell);
                }
                celllist.add(childcellList);
            }
            tableViewAdapter.setAllItems(headerlist, rowHeaderlist, celllist);
        } catch (JSONException e) {
        } catch (Exception e) {
        }
        builder.setCustomView(dialogView);
        builder.setConfirmButton("CANCEL", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        builder.show();
    }

    public void onClickUndoPickConfirmation() {
        final SweetAlertDialog builder = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Are you Sure ?")
                .setCustomImage(R.drawable.alertvoid)
                .setContentText("You Cannot Roll back Once you Undo !")
                .setConfirmText("YES! UNDO")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        undoAlltheWeights();
                    }
                });
        builder.setCancelText("CANCEL")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
        builder.show();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected)
            showSnack(isConnected);
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            showSnack(isConnected);
            if (noInternetDialog != null ? !noInternetDialog.isShowing() : false) {
                noInternetDialog.show();
            }
        }
        return isConnected;
    }

    @Override
    public void onPause() {
        Bungee.zoom(this);
        MyApplication.getInstance().setConnectivityListener(this);

        super.onPause();
    }

    @Override
    protected void onResume() {
        Bungee.zoom(this);
        MyApplication.getInstance().setConnectivityListener(this);

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            try {
                showSnack(isConnected);
                if (noInternetDialog != null ? !noInternetDialog.isShowing() : false) {
                    noInternetDialog.show();
                }
            } catch (Exception e) {
            }
        }

        if (myThreadConnectBTdevice != null) {
            myThreadConnectBTdevice.cancel();
        }
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        if (isConnected) {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.SUCCESS, "Connection Established!");
        } else {
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Connection couldn't Established!\nCheck the Internet Connection");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onFinishAcivity();
            return true;
        } else if (id == R.id.action_refresh) {
            indexOfList = null;
            if (isShowAll) {
                setListforShowAllView();
            } else {
                getHeaderDetails();
                lv.setLayoutAnimation(animation);
            }
            return true;
        } else if (id == R.id.action_header) {
            onClickHeaderDeatils();
            return true;
        } else if (id == R.id.action_lines) {
            getLinesDetails();
            return true;
        } else if (id == R.id.action_orderClearAll) {
            onClickUndoPickConfirmation();
            return true;
        } else if (id == R.id.action_bluetooth) {
            indexOfList = null;
            isAddMore = false;
            resetBluetoothConn(false);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();

        //Turn ON BlueTooth if it is OFF
        if (bluetoothAdapter != null) {
            if (!bluetoothAdapter.isEnabled()) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                Boolean syncConnPref = sharedPref.getBoolean("pref_bluetoothon", true);
                if (syncConnPref) {
                    bluetoothAdapter.enable();
                } else {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                }
            }
        } else {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean syncConnPref = sharedPref.getBoolean("pref_bluetoothon", true);
            if (syncConnPref) {
                bluetoothAdapter.enable();
            } else {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void setup() {
        dialog.setCancelable(true);
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            pairedDeviceArrayList = new ArrayList<BluetoothDevice>();
            ArrayList<String> pairedDeviceArrayNames = new ArrayList<String>();

            for (BluetoothDevice device : pairedDevices) {
                pairedDeviceArrayList.add(device);
                pairedDeviceArrayNames.add(device.getName());
            }

            ArrayAdapter<String> itemsusp = new ArrayAdapter<String>(WMSPickCard.this,
                    android.R.layout.simple_list_item_1, android.R.id.text1, pairedDeviceArrayNames);
            listViewPairedDevice.setAdapter(itemsusp);

            listViewPairedDevice.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    BluetoothDevice device =
                            (BluetoothDevice) pairedDeviceArrayList.get(position);
                    MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.INFO, "Name: " + device.getName() + "\n"
                            + "Address: " + device.getAddress());
                    myThreadConnectBTdevice = new WMSPickCard.ThreadConnectBTdevice(device);
                    myThreadConnectBTdevice.start();
                }
            });
        } else {
            if (dialog.isShowing())
                dialog.dismiss();
            MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.WARNING, "No Bluetooth Paired Devices, Please Make sure that device should be paired!");
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            onFinishAcivity();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE) {
            if (resultCode == RESULT_OK) {
                ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (matches != null && matches.size() > 0) {
                    String searchWrd = matches.get(0);
                    if (!TextUtils.isEmpty(searchWrd)) {
                        searchView.setQuery(searchWrd, false);
                    }
                }
            }
            return;
        } else if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                setup();
            } else {
                final SweetAlertDialog builder = new SweetAlertDialog(WMSPickCard.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("Bluetooth Setup")
                        .setCustomImage(R.drawable.alertexit)
                        .setContentText("BlueTooth NOT enabled, Are you sure want to EXIT");
                builder.setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                finish();
                            }
                        });
                builder.setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                Intent tender = new Intent(WMSPickCard.this, WMSGRCard.class);
                                startActivity(tender);
                            }
                        });
                builder.show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //Called in ThreadConnectBTdevice once connect successed
    //to start ThreadConnected
    private void startThreadConnected(BluetoothSocket socket) {


        myThreadConnected = new ThreadConnected(socket);
        myThreadConnected.start();
    }

    /*
    ThreadConnectBTdevice:
    Background Thread to handle BlueTooth connecting
    */
    private class ThreadConnectBTdevice extends Thread {

        private BluetoothSocket bluetoothSocket = null;
        private final BluetoothDevice bluetoothDevice;


        private ThreadConnectBTdevice(BluetoothDevice device) {
            bluetoothDevice = device;

            try {
                bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
                textStatus.animateText("Establishing Connection..");
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        AlertFailedSound();
                        Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }

        @Override
        public void run() {
            boolean success = false;
            try {
                bluetoothSocket.connect();
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
                success = false;
                final String eMessage = e.getMessage();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        AlertFailedSound();
                        MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Unable to Connect: " + eMessage);
                    }
                });
            }

            if (success) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        textInfo.setText(bluetoothDevice.getName());
                        AlertSound();
                        if (indexOfList != null) {
                            if (!dialog.isShowing())
                                setBletooth();
                        }
                    }
                });
                startThreadConnected(bluetoothSocket);
            } else {
                try {
                    if (indexOfList != null) {
                        if (!dialog.isShowing())
                            setBletooth();
                    }
                } catch (Exception e) {
                }
            }
        }

        public void cancel() {
            if (bluetoothSocket != null) {
                try {
                    bluetoothSocket.close();
                } catch (Exception e) {
                }
            }
            if (bluetoothSocket.isConnected()) {
                try {
                    bluetoothSocket.close();
                } catch (Exception e) {
                }
            }
        }

    }

    public void resetBluetoothConn(Boolean type) {
        if (type) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            type = sharedPref.getBoolean("pref_bluetooth_disconnect", false);
        } else {
            type = true;
        }
        if (type) {
            if (myThreadConnectBTdevice != null ? myThreadConnectBTdevice.bluetoothSocket != null : false) {
                try {
                    myThreadConnectBTdevice.cancel();
                    myThreadConnected.cancel();
                    myThreadConnectBTdevice.interrupt();
                    myThreadConnected.interrupt();
                    myThreadConnectBTdevice = null;
                    myThreadConnected = null;
                } catch (Exception e) {
                    if (!dialog.isShowing())
                        setBletooth();
                }
            } else {
                if (!dialog.isShowing())
                    setBletooth();
            }
        }
    }

    /*
    ThreadConnected:
    Background Thread to handle Bluetooth data communication
    after connected
     */
    private class ThreadConnected extends Thread {
        private final BluetoothSocket connectedBluetoothSocket;
        private final InputStream connectedInputStream;
        private final OutputStream connectedOutputStream;
        String displayValue = "";

        public ThreadConnected(BluetoothSocket socket) {
            connectedBluetoothSocket = socket;
            InputStream in = null;
            OutputStream out = null;

            try {
                in = socket.getInputStream();
                out = socket.getOutputStream();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            connectedInputStream = in;
            connectedOutputStream = out;

        }

        @Override
        public void run() {
            byte[] buffer = new byte[512];
            int bytes;
            boolean isContinue = true;

            while (isContinue) {
                try {
                    try {
                        sleep(3000);
                    } catch (InterruptedException e) {
                    }
                    if (connectedInputStream != null && myThreadConnectBTdevice.bluetoothSocket.isConnected()) {
                        bytes = connectedInputStream.read(buffer);
                        String strReceived = new String(buffer, 0, bytes);
                        final String[] parts = strReceived
                                .replaceAll("[^\\d.]", " ")
                                .trim()
                                .split("[\\n\\t\\r ]+");

                        if (parts.length >= 2) {
                            if (!displayValue.equals(parts[1])) {
                                displayValue = parts[1];
                                msgReceived = displayValue;

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        AlertSound();
                                        textStatus.animateText(displayValue);
                                    }
                                });
                            }
                        } else if (parts.length >= 1) {
                            if (!displayValue.equals(parts[0])) {
                                displayValue = parts[0];
                                msgReceived = displayValue;

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        AlertSound();
                                        textStatus.animateText(displayValue);
                                    }
                                });
                            }
                        }
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                    isContinue = false;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (!isFinishing()) {
                                AlertFailedSound();
                                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Connection lost!");
                            }
                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();

                    isContinue = false;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (!isFinishing()) {
                                AlertFailedSound();
                                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.ERROR, "Connection lost!");
                            }
                        }
                    });
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                connectedOutputStream.write(buffer);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                connectedBluetoothSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void AlertSound() {
        if (dialog.isShowing()) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            if (sharedPref.getBoolean("pref_notification", true)) {
                try {
                    toneGen1.startTone(ToneGenerator.TONE_SUP_ERROR, 300);
                } catch (Exception e) {
                }
            }
        } else {
            if (indexOfList == null) {
                MyApplication.displaySnackBar(WMSPickCard.this, MyApplication.SUCCESS, "Successfully Connected!");
            }
        }
    }

    public void AlertFailedSound() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPref.getBoolean("pref_notification", true)) {
            try {
                toneGen1.startTone(ToneGenerator.TONE_SUP_ERROR, 300);
            } catch (Exception e) {
            }
        }
    }

    public void onFinishAcivity() {
        final SweetAlertDialog builder = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setTitleText("Are you sure ?")
                .setCustomImage(R.drawable.alertexit)
                .setContentText("Back to List ?");
        builder.setConfirmText("YES BACK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        MyStatic.setWhichactivity("PICKORDER");
                        startActivityForResult(new Intent(WMSPickCard.this, HomeViewActivity.class), 1);
                        setResult(2);
                        finish();
                    }
                });
        builder.setCancelText("CANCEL")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });
        builder.show();
    }
}
