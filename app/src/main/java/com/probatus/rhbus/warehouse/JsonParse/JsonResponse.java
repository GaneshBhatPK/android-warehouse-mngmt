package com.probatus.rhbus.warehouse.JsonParse;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.probatus.rhbus.warehouse.MyApplication;
import com.probatus.rhbus.warehouse.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by ganapathi on 12/4/19.
 */

public class JsonResponse {

    public void JsonResponse(
            final Context thisC,
            String serviceName,
            final String methodName,
            ArrayList<String> list,
            final HttpResponse<JSONObject> callback1){

        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParam = null;
        try {
            jsonParam = new JSONObject();
            jsonParam.put("serviceName", serviceName);
            jsonParam.put("methodName", methodName);
            jsonParam.put("parameters", new JSONArray(list));
        }catch (JSONException e){}
        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonParam.toString());
        }catch (IOException e){}

        client.post(thisC, MyApplication.getInstance().getSomeVariable().get(3).toString() + thisC.getResources().getString(R.string.hitPhpPathCommon), entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                Log.e("Tag","Started");
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode,headers,response);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("result", response);
                    if (callback1 != null) {
                        callback1.onResponse(jsonObject);
                    }
                }catch (JSONException e){
                    Log.e("Tag",""+e);
                }
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode,headers,response);
                if (callback1 != null) {
                    try {
                        callback1.onResponse(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString){
                super.onSuccess(statusCode,headers,responseString);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("result", responseString);
                    if (callback1 != null) {
                        callback1.onResponse(jsonObject);
                    }
                }catch (JSONException e){
                    Log.e("Tag",""+e);
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse){
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("result", "Please Try Again After Some Time, Error Status: " + statusCode + "/"+ throwable);
                    writeToFile("Status : "+ statusCode +
                            "headers : "+ headers +
                            "Throwable : " + throwable +
                            "JSONArray : "+ errorResponse, thisC);
                    if (callback1 != null) {
                        callback1.onResponse(jsonObject);
                    }
                }catch (JSONException e){}
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                super.onFailure(statusCode, headers,  throwable, errorResponse);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("result", "Please Try Again After Some Time, Error Status: " + statusCode + "/"+ throwable);
                    writeToFile("Status : "+ statusCode +
                            "headers : "+ headers +
                            "Throwable : " + throwable +
                            "JSONObject : "+ errorResponse, thisC);
                    if (callback1 != null) {
                        callback1.onResponse(jsonObject);
                    }
                }catch (JSONException e){}
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable){
                super.onFailure(statusCode, headers, responseString, throwable);
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("result", "Please Try Again After Some Time, Error Status: " + statusCode + "/"+ throwable);
                    writeToFile("Status : "+ statusCode +
                            "headers : "+ headers +
                            "Throwable : " + responseString +
                            "responseString : "+ responseString, thisC);
                    if (callback1 != null) {
                        callback1.onResponse(jsonObject);
                    }
                }catch (JSONException e){}
            }

            @Override
            public boolean getUseSynchronousMode() {
                return false;
            }

            @Override
            protected  Object parseResponse(byte[] responseBody) throws JSONException {
                if (null == responseBody)
                    return null;
                Object result = null;
                //trim the string to prevent start with blank, and test if the string is valid JSON, because the parser don't do this :(. If JSON is not valid this will return null
                String jsonString = getResponseString(responseBody, getCharset());
                if (jsonString != null) {
                    jsonString = jsonString.trim();
                    if (jsonString.startsWith(UTF8_BOM)) {
                        jsonString = jsonString.substring(1);
                    }
                    if (jsonString.startsWith("{") || jsonString.startsWith("[")) {
                        result = new JSONTokener(jsonString).nextValue();
                    }
                }
                if (result == null) {
                    result = jsonString;
                    Log.e("ERROR",""+result);
                }
                return result;
            }
        });
    }

    public void writeToFile(String data,Context context) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "RHBUS");
            if (!root.exists()) {
                root.mkdirs();
            }
            root = new File(Environment.getExternalStorageDirectory() + "/RHBUS/", "Logs");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "NetworkLog.txt");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer = new FileWriter(gpxfile, false);
            writer.write(data);
            writer.flush();
            writer.close();
        } catch (IOException e) {
        } catch (Exception e) {
        }
    }
}
