package com.probatus.rhbus.warehouse.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.SearchView;

import com.labo.kaji.fragmentanimations.CubeAnimation;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.probatus.rhbus.warehouse.Adapter.DataAdapter;
import com.probatus.rhbus.warehouse.HomeViewActivity;
import com.probatus.rhbus.warehouse.Interface.ItemClickListener;
import com.probatus.rhbus.warehouse.Interface.productversion;
import com.probatus.rhbus.warehouse.JsonParse.HttpResponse;
import com.probatus.rhbus.warehouse.JsonParse.MyStatic;
import com.probatus.rhbus.warehouse.MyApplication;
import com.probatus.rhbus.warehouse.R;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by ganapathi on 29/10/18.
 */

public class WMSGRListFragment extends Fragment implements
        ItemClickListener,SearchView.OnQueryTextListener {

    RecyclerView recyclerView;
    SwipeRefreshLayout refreshCollection;
    LayoutAnimationController animation;
    RecyclerViewFastScroller recyclerViewFastScroller;
    public productversion productversionsList;
    DataAdapter adapter;
    public static ArrayList<productversion> productversions= new ArrayList<>();
    public ArrayList<productversion> product_version = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.listview_render, container, false);

        refreshCollection =(SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        recyclerViewFastScroller = (RecyclerViewFastScroller) view.findViewById(R.id.fast_scroller);

        animation = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

        refreshCollection =(SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        refreshCollection.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPlease(true);
            }
        });

        initViews();

        return view;
    }

    public void initViews(){
        refreshCollection.setRefreshing(true);
        productversions = new ArrayList<>();

        if(((HomeViewActivity) getActivity()).checkConnection()) {
            prepareData();
        }
    }

    private ArrayList<productversion> prepareData(){
        ArrayList<String> list = new ArrayList<String>();
        list.add(MyStatic.getTokenKey());
        product_version = new ArrayList<>();
        ((HomeViewActivity) getActivity()).jsonResponse.JsonResponse(getActivity(), "wmsGRCard_android", "getAllGR", list, new HttpResponse<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    JSONArray array = response.getJSONArray("result");
                    String[] suggestion = new String[array.length()];
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject c = array.getJSONObject(i);
                        productversion productversion = new productversion();
                        productversion.getParameterNo_7(c.getString("DocumentNo"));

                        String code = isNullChecker(c.getString("Code"));
                        String name = isNullChecker(c.getString("Name"));
                        String address = isNullChecker(c.getString("SourceNo"));
                        String location = isNullChecker(c.getString("LocationCode"));
                        String address2 = isNullChecker(c.getString("Address2"));

                        productversion.setParameterNo_1(name);
                        productversion.setParameterNo_2(c.getString("DocumentNo") + name + address + address2);
                        suggestion[i] = c.getString("DocumentNo");
                        productversion.setParameterNo_13(c.getString("ItemCount") + " Received Lines :"+ c.getString("LineCount"));
                        productversion.setParameterNo_16(c.getString("DocumentDate"));
                        productversion.setParameterNo_14(address + " Location :" + location);
                        productversion.setParameterNo_15(address2);
                        productversion.setAndroid_image_url(null);
                        productversion.setParameterNo_3(getDocStatus(c));
                        product_version.add(productversion);
                    }
                    ((HomeViewActivity) getActivity()).setSearchViewShown(suggestion);
                }catch (Exception e) {
                    MyApplication.displaySnackBar(getActivity(),MyApplication.ERROR, ""+ e);
                }
                setting();
            }
        });
        return product_version;
    }

    public String isNullChecker(String userEmail){
        if(userEmail != null && (!TextUtils.equals(userEmail ,"null")) && (!TextUtils.isEmpty(userEmail))){
            return userEmail;
        } else {
            return "-";
        }
    }

    public String getDocStatus(JSONObject c){
        try {
            if(c.getDouble("LineCount") == c.getDouble("ItemCount")){
                return "1";
            } else if(c.getDouble("LineCount") == 0){
                return "2";
            }
            return "3";
        } catch (Exception e){
            return "3";
        }
    }

    @Override
    public void onClick(final View view, int posi) {
        if(productversions.size() == 0) {
            initViews();
        } else {
            productversionsList = product_version.get(posi);
            MyApplication.getInstance().setDocumentNumber(productversionsList.getParameterNo_7());
            MyStatic.setProdProcess(productversionsList.getParameterNo_2());
            MyStatic.setLineNumber(productversionsList.getParameterNo_3());
            ((HomeViewActivity) getActivity()).transferToNextForGR();
        }
    }


    private void setting(){
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLayoutAnimation(animation);
        productversions = product_version;
        adapter = new DataAdapter(getActivity(), productversions);
        recyclerView.setAdapter(adapter);
        adapter.setClickListener(this);
        this.refreshPlease(false);
    }

    public void refreshPlease(boolean isrefresh){
        if(isrefresh){
            initViews();
        } else {
            if (productversions.size() > 0) {
                ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
                List<String> strAlphabets = new ArrayList<>();
                for (int i = 0; i < productversions.size(); i++) {
                    String name = productversions.get(i).getParameterNo_15();
                    if (name == null || name.trim().isEmpty())
                        continue;

                    String word = name.substring(0, 1);
                    if (!strAlphabets.contains(word)) {
                        strAlphabets.add(word);
                        mAlphabetItems.add(new AlphabetItem(i, word, false));
                    }
                }
                recyclerViewFastScroller.setRecyclerView(recyclerView);
                recyclerViewFastScroller.setUpAlphabet(mAlphabetItems);
            }
            refreshCollection.setRefreshing(false);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if(adapter != null)
            adapter.filter(query,1);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(adapter != null)
            adapter.filter(newText,1);
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        //initViews();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        if (enter) {
            return MoveAnimation.create(MoveAnimation.UP, enter, 500);
        } else {
            return CubeAnimation.create(CubeAnimation.UP, enter, 500);
        }
    }
}
