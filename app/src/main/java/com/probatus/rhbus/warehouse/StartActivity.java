package com.probatus.rhbus.warehouse;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.dcastalia.localappupdate.DownloadApk;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.probatus.rhbus.warehouse.Interface.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import cz.msebera.android.httpclient.Header;
import lecho.cn.pedant.SweetAlert.SweetAlertDialog;
import me.wangyuwei.particleview.ParticleView;
import spencerstudios.com.bungeelib.Bungee;


public class StartActivity extends AppCompatActivity implements
        ConnectivityReceiver.ConnectivityReceiverListener {

    SQLitedbHelper myDb;
    boolean dbexists = false;
    private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1001;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.splash_screen);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(StartActivity.this);
        myDb = SQLitedbHelper.getInstance(StartActivity.this);
        dbexists = myDb.checkDatabase();
        myDb.close();

        ParticleView mParticleView = (ParticleView) findViewById(R.id.mParticleView);
        mParticleView.startAnim();

        mParticleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                if (sharedPref.getBoolean("pref_notification_updates", true)) {
                    if (checkConnection()) {
                        onUpdateChecker();
                    } else {
                        startNextAcivity();
                    }
                } else {
                    startNextAcivity();
                }
            }
        });

    }

    public void startNextAcivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dbexists) {
                    MyApplication.getInstance().setFirsttimeStatus(false);
                    StartActivity.this.startActivity(new Intent(StartActivity.this, LoginActivity.class));
                } else {
                    MyApplication.getInstance().setFirsttimeStatus(true);
                    StartActivity.this.startActivity(new Intent(StartActivity.this, MyIntro.class));
                }
            }
        }, 100);
    }

    public void onUpdateChecker() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(StartActivity.this, StartActivity.this.getResources().getString(R.string.hitgithubCommon),
                null, new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            PackageInfo pInfo = StartActivity.this.getPackageManager().getPackageInfo(StartActivity.this.getPackageName(), 0);
                            if (response.getString("latestVersion").compareTo(pInfo.versionName) > 0) {
                                SweetAlertDialog builder = new SweetAlertDialog(StartActivity.this,
                                        SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Update Available: " + response.getString("latestVersion"))
                                        .setContentText(response.getString("releaseVersion"))
                                        .setConfirmText("UPDATE")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                checkWriteExternalStoragePermission();
                                            }
                                        });
                                builder.setCancelText("SKIP")
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                startNextAcivity();
                                            }
                                        });
                                builder.setNeutralText("DISABLE")
                                        .setNeutralClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                                sharedPref.edit().putBoolean("pref_notification_updates", false).commit();
                                                startNextAcivity();
                                            }
                                        });
                                builder.setCancelable(false);
                                builder.show();
                            } else {
                                startNextAcivity();
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            startNextAcivity();
                        } catch (JSONException e) {
                            startNextAcivity();
                        }
                    }
                });
    }

    private void checkWriteExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            /** If we have permission than we can Start the Download the task **/
            downloadTask();
        } else {
            /** If we don't have permission than requesting  the permission **/
            requestWriteExternalStoragePermission();
        }
    }

    private void requestWriteExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            downloadTask();
        } else {
            Toast.makeText(this, "Permission Not Granted.", Toast.LENGTH_SHORT).show();
            startNextAcivity();
        }
    }

    private void downloadTask() {
        /** This @DownloadApk class is provided by our Library **/
        /** Pass the  Context when creating object of DownlodApk **
         */

        DownloadApk downloadApk = new DownloadApk(this);

        /** For Starting download call the method startDownLoadingApk() by passing the URL **/
        downloadApk.startDownloadingApk(this.getResources().getString(R.string.hitgithubdownload));
    }

    @Override
    public void onPause() {
        super.onPause();
        Bungee.zoom(this);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            showSnack(isConnected);
        }
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            showSnack(isConnected);
        }
    }

    private void showSnack(boolean isConnected) {
        if (isConnected) {
            MyApplication.displaySnackBar(StartActivity.this, MyApplication.SUCCESS, "Connection Established!");
        } else {
            MyApplication.displaySnackBar(StartActivity.this, MyApplication.ERROR, "Connection couldn't Established!\nCheck the Internet Connection");
        }
    }
}
