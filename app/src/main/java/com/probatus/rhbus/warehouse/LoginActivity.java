package com.probatus.rhbus.warehouse;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Spinner;

import com.probatus.rhbus.warehouse.Interface.ExceptionHandler;
import com.probatus.rhbus.warehouse.JsonParse.HttpResponse;
import com.probatus.rhbus.warehouse.JsonParse.JsonResponse;
import com.probatus.rhbus.warehouse.JsonParse.MyStatic;
import com.zigis.materialtextfield.MaterialTextField;

import net.igenius.customcheckbox.CustomCheckBox;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import am.appwise.components.ni.NoInternetDialog;
import lecho.cn.pedant.SweetAlert.SweetAlertDialog;
import mehdi.sakout.fancybuttons.FancyButton;
/**
 * Created by raksha on 14/3/18.
 */

public class LoginActivity extends Activity implements ConnectivityReceiver.ConnectivityReceiverListener {

    SQLitedbHelper mydb;
    MaterialTextField eduser,edpass, edCustKey, customerEnd;
    Cursor d=null;
    CustomCheckBox scb;
    Spinner staticSpinner;
    FancyButton btn;
    SweetAlertDialog builder;

    NoInternetDialog noInternetDialog;
    SharedPreferences prefs;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        setContentView(R.layout.activity_login);
        MyStatic.setmContext(this);

        edCustKey = (MaterialTextField) findViewById(R.id.customerkey);
        customerEnd = (MaterialTextField) findViewById(R.id.customerEnd);
        eduser = (MaterialTextField) findViewById(R.id.username);
        edpass = (MaterialTextField) findViewById(R.id.password);
        scb = (CustomCheckBox) findViewById(R.id.scb);

        staticSpinner = (Spinner) findViewById(R.id.spinner1);

        prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        if(prefs.getString("sync_http",
                getString(R.string.httpsRequest)).equals(getString(R.string.httpsRequest))){
            staticSpinner.setSelection(1, true);
        } else {
            staticSpinner.setSelection(0, true);
        }
        scb.setChecked(true, true);
        edpass.requestFocus();

        btn = (FancyButton) findViewById(R.id.button2);

        mydb = SQLitedbHelper.getInstance(getApplicationContext());
        d = mydb.fetchUSERInfo();
        d.moveToFirst();
        eduser.setText(d.getString(0));
        edCustKey.setText(d.getString(1));
        customerEnd.setText(d.getString(2));

        scb.setOnCheckedChangeListener(new CustomCheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CustomCheckBox checkBox, boolean isChecked) {
                if(isChecked) {
                    saveRememberMe();
                }
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scb.isChecked()) {
                    saveRememberMe();
                }
                if (!TextUtils.isEmpty(customerEnd.getText().toString())) {
                    if (!TextUtils.isEmpty(edpass.getText().toString())) {
                        if (!TextUtils.isEmpty(eduser.getText().toString())) {
                            if (!TextUtils.isEmpty(edCustKey.getText().toString())) {
                                ArrayList<String> someVariable = new ArrayList<String>();
                                String url = prefs.getString("sync_http", staticSpinner.getSelectedItem().toString()) + customerEnd.getText().toString();
                                someVariable.add(eduser.getText().toString());
                                someVariable.add(edpass.getText().toString());
                                someVariable.add(edCustKey.getText().toString());
                                someVariable.add(url);
                                MyApplication.getInstance().setSomeVariable(someVariable);
                                if (checkConnection()) {
                                    builder = new SweetAlertDialog(LoginActivity.this,
                                            SweetAlertDialog.PROGRESS_TYPE)
                                            .setTitleText("Loading");
                                    builder.setCancelable(false);
                                    builder.show();

                                    try {
                                        authenticateRequest();
                                    } catch (Exception e) {
                                        if (builder.isShowing())
                                            builder.dismissWithAnimation();
                                        MyApplication.displaySnackBar(LoginActivity.this, MyApplication.ERROR,
                                                "End Point is Not Correct/Valid!!");
                                    }
                                }
                            } else {
                                edCustKey.setError("Customer Key is Empty!!");
                                edCustKey.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.shake));
                            }
                        } else {
                            eduser.setError("User Name is Empty!!");
                            eduser.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.shake));
                        }
                    } else {
                        edpass.setError("Password is Empty!!");
                        edpass.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.shake));
                    }
                }else {
                    customerEnd.setError("IP/End Point is Empty!!");
                    customerEnd.startAnimation(AnimationUtils.loadAnimation(getApplication(), R.anim.shake));
                }
            }
        });

        noInternetDialog = new NoInternetDialog.Builder(this).build();
    }

    public void saveRememberMe() {
        if (!TextUtils.isEmpty(eduser.getText().toString())) {
            if (!TextUtils.isEmpty(edCustKey.getText().toString())) {
                if (!TextUtils.isEmpty(customerEnd.getText().toString())) {
                    mydb.updateUSERInfo(eduser.getText().toString(),
                            edCustKey.getText().toString(), customerEnd.getText().toString());
                }
            }
        }
    }

    public void authenticateRequest() {
        JsonResponse jsonRe = new JsonResponse();
        ArrayList<String> list = new ArrayList<String>();
        list.add(MyApplication.getInstance().getSomeVariable().get(2).toString());
        list.add(MyApplication.getInstance().getSomeVariable().get(0).toString());
        list.add(MyApplication.getInstance().getSomeVariable().get(1).toString());
        edpass.setText("");
        jsonRe.JsonResponse(LoginActivity.this, "Auth3", "inCreds", list,
                new HttpResponse<JSONObject>() {
                    public void onResponse(JSONObject response) throws JSONException {
                        if (builder.isShowing())
                            builder.dismissWithAnimation();
                        try {
                            JSONObject jsonObj = response.getJSONArray("data").getJSONObject(0);
                            MyStatic.setTokenKey(jsonObj.getString("accesstoken"));
                            MyStatic.setProdProcess(jsonObj.getString("RoleID"));
                            LoginActivity.this.startActivity(new Intent(LoginActivity.this, UserActivity.class));
                        } catch (Exception e) {
                            MyApplication.displaySnackBar(LoginActivity.this,MyApplication.ERROR, ""+ response.getString("result"));
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            showSnack(isConnected);
        }
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            showSnack(isConnected);
            if (noInternetDialog != null ? !noInternetDialog.isShowing() : false) {
                noInternetDialog.show();
            }
        }
        return isConnected;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(noInternetDialog != null) {
            noInternetDialog.onDestroy();
        }
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        if (isConnected) {
            MyApplication.displaySnackBar(LoginActivity.this,MyApplication.SUCCESS, "Connection Established!");
        } else {
            MyApplication.displaySnackBar(LoginActivity.this, MyApplication.ERROR, "Connection couldn't Established!\nCheck the Internet Connection");
        }
    }
}
