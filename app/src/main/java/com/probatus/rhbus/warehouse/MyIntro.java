package com.probatus.rhbus.warehouse;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by ganesh on 4/7/18.
 */

public class MyIntro extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("WAREHOUSE MANAGEMENT APP",
                "Best plateform to grow your Bussiness\nProvides Easy and Smooth Way,\nWant To Know How? Here we Go....",
                R.drawable.splash_production, R.drawable.back_slide));
        addSlide(AppIntroFragment.newInstance("WAREHOUSE DESIGN",
                "Which enables organizations to customize workflow and picking logic\n to make sure that the warehouse is designed for Optimized Inventory Allocation",
                R.drawable.splash_infographic, R.drawable.back_slide));
        addSlide(AppIntroFragment.newInstance("INVENTORY TRACKING",
                "Do All Production Activity in single Application\n that too in your Hand",
                R.drawable.splash_planning, R.drawable.back_slide));
        addSlide(AppIntroFragment.newInstance("PICKING PACKING GOODS",
                "Including Zone,Location picking,\n Warehouse workers can also use lot zoning and\n task interleaving functions to guide the pick-and-pack\n tasks in the most Efficient way",
                R.drawable.user_salesorder, R.drawable.back_slide));
        addSlide(AppIntroFragment.newInstance("DATA SECURITY",
                "Highly Available, Protected and Secured Data with\n Cloud Based Integrated WMS and ERP System..",
                R.drawable.splash_report1, R.drawable.back_slide));


        showSkipButton(false);
        setBarColor(Color.parseColor("#3F51B5"));
        setProgressButtonEnabled(true);
        setFlowAnimation();

        try {
            askForPermissions(new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.REQUEST_INSTALL_PACKAGES }, 1);
        }catch (Exception e){
            Log.e("Intro",""+e);
        }
    }

    @Override
    public void onSkipPressed() {
        super.onSkipPressed();
        this.startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

    @Override
    public void onDonePressed() {
        super.onDonePressed();
        this.startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        showSkipButton(true);
    }

    @Override
    public void onPause(){
        super.onPause();
    }
}
